import java.util.ArrayList;
import java.util.Scanner;

public class Raft {

	static char arr[][] = new char[5][5];
	static ArrayList<String> arr2 = new ArrayList<String>();
	static String num = " ";

	public static boolean path(int x, int y) {
		if (x < 0 || y < 0) {
			return false;
		}
		
		if(x >= 5 || y >= 5) {
			return false;
		}
		
		if (num.length() == 6) {
			int count = 0;
			for(int i = 0; i < arr2.size(); i++){
				 if(num.equals(arr2.get(i))){
					    arr2.remove(i);
					    num = " ";
					 	num = num + arr[x][y];
						arr2.add(num);
						count+=1;
					    return true;
				 }else if(!num.equals(arr2.get(i))) {
					 	num = num + arr[x][y];
						arr2.add(num);
						num = " ";
						count+=1;
						return true;
				 }
			}
			if (count == 0) {
				num = num + arr[x][y];
				arr2.add(num);
				num = " ";
				return true;
			}
		}

		num += arr[x][y];
		
		if (path(x - 1, y)) {
			return true;
		}
		
		if (path(x + 1, y)) {
			return true;
		}
		
		if (path(x, y - 1)) {
			return true;
		}
		
		if (path(x, y + 1)) {
			return true;
		}
		
		num = num.substring(0, num.length()-1);

		return false;
	}


	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = sc.next().charAt(0);
			}
		}
		for(int i = 0; i< arr.length; i++) {
			for(int j = 0; j< arr[i].length; j++) {
				path(i, j);
			}
		}
		System.out.println(arr2.size());
	}

}