import java.util.Scanner;

public class Knapsack {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int m = sc.nextInt();
		int[] pi = new int[n+1];
		int[] wi = new int[n+1];
		
		for(int i=1; i<=n; i++) {
				pi[i]=sc.nextInt();
				wi[i]=sc.nextInt();
		}
		
		System.out.println(knapSack(pi, wi, pi.length - 1, m));
	}
	
	static int knapSack(int[] pi, int[] wi, int n, int m) {
		
		if (m < 0) {
			return Integer.MIN_VALUE;
		}

		if (n < 0 || m == 0) {
			return 0;
		}

		int include = pi[n] + knapSack(pi, wi, n - 1, m - wi[n]);

		int exclude = knapSack(pi, wi, n - 1, m);

		return Integer.max(include, exclude);
	}

}
