import java.util.Arrays;
import java.util.Scanner;

public class test {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int c = sc.nextInt();
		int sum = sc.nextInt();
		int[] coin = new int[c+1];
		
		for(int i=1; i<=c; i++) {
			coin[i]=sc.nextInt();
		}
		
		Arrays.sort(coin);
		
		System.out.println(coinChange(coin, sum));
	}
	
	public static int coinChange(int[] coins, int sum) {
	    if(sum==0){
	        return 0;
	    }
	 
	    int[] change = new int[sum+1];
	 
	    Arrays.fill(change, Integer.MAX_VALUE);
	    change[0] = 0;
	 
	    for(int i=0; i<=sum; i++){
	        if(change[i]==Integer.MAX_VALUE){
	            continue;
	        }
	 
	        for(int coin: coins){
	            if(i<=sum-coin){ 
	            	change[i+coin] = Math.min(change[i]+1, change[i+coin]);
	            }
	        }
	    }
	 
	    if(change[sum]==Integer.MAX_VALUE){
	        return -1;
	    }
	 
	    return change[sum];
	}
}
