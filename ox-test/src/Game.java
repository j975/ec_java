import java.util.Scanner;

public class Game {

    private Board board;
    private int row, col;
    private Player o, x;
    public int count;

    Scanner sc = new Scanner(System.in);

    public Game() {
        row = 0;
        col = 0;
        count = 0;
    }

    public void startGame() {
        o = new Player('O');
        x = new Player('X');
        board = new Board(o, x);
    }

    public void showWelcome() {
        System.out.println("Welcome To OX Game");
    }

    public void showBoard(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public void showTurn(Player p) {
        System.out.println("Turn" + p.getName());
    }

    public void showStat() {
        System.out.println(o.getName() + ": Win:" + o.getWin() + " Lose:" + o.getLose() + " Draw:" + o.getDraw());
        System.out.println(x.getName() + ": Win:" + x.getWin() + " Lose:" + x.getLose() + " Draw:" + x.getDraw());
    }

    public boolean input() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("Input your Row Col :");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;

            if (board.setRowCol(row, col) == true) {
                return true;
            } else {
                System.out.println("Input try again...");
                return false;
            }
        } catch (Exception e) {
            System.out.print("Input try again...");
            sc.nextLine();
            return false;
        }
    }

    public void showWin(Player p) {
        System.out.println("Player " + p.getName() + " Win!");
    }

    public void showDraw() {
        System.out.println("Draw!!");
    }

    public boolean inputContinue() {
        System.out.print("Continue (Yes/No) : ");
        String incont = sc.next();
        if (incont.equals("Yes") || incont.equals("Y") || incont.equals("y")) {
            return true;
        }
        return false;
    }

    public void showBye() {
        System.out.println("Bye Bye !!");
    }

    public void runOnec() {
        while (true) {
            this.showBoard(board.getData());
            showTurn(board.currentPlayer);
            count++;
            if (this.input() == false) {
                continue;
            }
            if (board.checkWin(board.getCurrentPlayer(), row, col) == true) {
                Player player = board.getWinner();
                this.showBoard(board.getData());
                this.showWin(player);
                this.showStat();
                return;
            } else if (board.checkDraw(count) == true) {
                o.countDraw();
                x.countDraw();
                return;

            }
            board.swithTurn();
        }
    }

    public void run() {
        showWelcome();
        do {
            startGame();
            this.runOnec();
        } while (inputContinue() == true);
        showStat();
        showBye();
        return;
    }
}