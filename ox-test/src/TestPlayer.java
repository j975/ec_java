import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestPlayer {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testConstuctor() {
		Player x = new Player('x');
		assertEquals(true, x.getName() == 'x');
	}

	@Test
	void testAddWin() {
		Player x = new Player('x');
		x.countWin();
		assertEquals(true, x.getWin() == 1);
	}
	
	@Test
	void testCountWin2() {
		Player o = new Player('O');
		o.countWin();
		assertEquals(true, o.getWin() == 1);
	}
	
	@Test
	void testCountDraw() {
		Player x = new Player('X');
		x.countDraw();
		assertEquals(true, x.getDraw() == 1);
	}
	
	
	@Test
    public void testCheckCross1() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board board = new Board(o,x);
		try {
			board.setRowCol(1, 1);
			board.setRowCol(2, 2);
			board.setRowCol(3, 3);
			assertEquals(true, board.checkWin(x, 0, 0));
		}catch (Exception e) {
			
		}
		
    }
	
	@Test
    public void testCheckRow() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board board = new Board(o,x);
		try {
			board.setRowCol(1, 0);
			board.setRowCol(1, 1);
			board.setRowCol(1, 2);
		}catch (Exception e) {
			assertEquals(true, board.checkWin(x, 0, 0));
		}
		
    }
	
	@Test
    public void testCheckCol() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board board = new Board(o,x);
		try {
			board.setRowCol(0, 2);
			board.setRowCol(1, 2);
			board.setRowCol(2, 2);
		}catch (Exception e) {
			assertEquals(true, board.checkWin(x, 0, 0));
		}
		
    }
	
	@Test
	public void testSetRowCol() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board board = new Board(o,x);
		assertEquals(true, true);
	}
	
	@Test
	public void testRandomPlayer(){
		Player o = new Player('O');
		Player x = new Player('X');
		assertEquals(true, true);
	}
	
	@Test
	public void testSetName() {
		Player o = new Player('O');
		Player x = new Player('X');
		assertEquals('O', o.getName());
		assertEquals('X', x.getName());
	}
	
	@Test
	public void testGetData(){
		Player o = new Player('O');
		Player x = new Player('X');
		Board board = new Board(o,x);
		char data[][] = board.getData();
		assertEquals('-',data[0][0]);
	}

}
