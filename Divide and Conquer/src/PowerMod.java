import java.util.Scanner;

public class PowerMod {

	public static int power(int a,int b,int c) {
		if(b == 0 ) {
			return 1;
		}
		
		int x = power(a, b/2, c);
		
		if(b % 2 == 0) {
			 x = power(a, b/2, c);
			return (x*x)%c;
		}else {
			 x = power(a, b/2, c);
			return (a*(x*x)%c)%c;
		}
		
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a,b,c;
		int num = sc.nextInt();
		for(int i=0; i<num; i++) {
			a = sc.nextInt();
			b = sc.nextInt();
			c = sc.nextInt();
			System.out.println(power(a, b, c));
		}
	}

}
